package chatroom

/* a basic chatroom implementation to get us started. Most of the code is directly copied from the revel samples */

import (
	"time"

	"github.com/revel/revel"
)
import "container/list"

type Event struct {
	Type      string
	UserModel      string
	Timestamp int
	Text      string
}

type Subscription struct {
	Archive []Event
	New     <-chan Event
}

type Chatroom struct {
	Name        string
	Archives    *list.List
	Subscribers *list.List
	subscribe   chan (chan<- Subscription)
	unsubscribe chan (<-chan Event)
	publish     chan Event
}

func (s Subscription) Cancel(cr *Chatroom) {
	cr.unsubscribe <- s.New
	drain(s.New)
}

func newEvent(typ, user, msg string) Event {
	return Event{typ, user, int(time.Now().Unix()), msg}
}

func (cr *Chatroom) Subscribe() Subscription {

	revel.INFO.Print("Subscribing to " + cr.Name)
	resp := make(chan Subscription)
	cr.subscribe <- resp

	revel.INFO.Print("Finished subscribing to " + cr.Name)
	return <-resp
}

func (cr *Chatroom) Join(user string) {

	revel.INFO.Print("UserModel " + user + " joining " + cr.Name)
	cr.publish <- newEvent("join", user, "")
}

func (cr *Chatroom) Say(user, message string) {

	revel.INFO.Print("UserModel " + user + " said " + message + " in " + cr.Name)
	cr.publish <- newEvent("message", user, message)
}

func (cr *Chatroom) Play(user, message string) {

	revel.INFO.Print("UserModel " + user + " played " + message + " in " + cr.Name)
	cr.publish <- newEvent("play", user, message)
}

func (cr *Chatroom) Stop(user, message string) {

	revel.INFO.Print("UserModel " + user + " stopped playing " + message + " in " + cr.Name)
	cr.publish <- newEvent("stop", user, message)
}

func (cr *Chatroom) Leave(user string) {

	revel.INFO.Print("UserModel" + user + "Leaving " + cr.Name)
	cr.publish <- newEvent("leave", user, "")
}

const archiveSize = 10

/*
var (
	subscribe   = make(chan (chan<- Subscription), 10)
	unsubscribe = make(chan (<-chan Event), 10)
	publish     = make(chan Event, 10)
)
*/

var Default = Make()

func (cr *Chatroom) RunChatroom() {

	revel.INFO.Print(" Running Chatroom " + cr.Name)

	for {
		select {
		case ch := <-cr.subscribe:
			var events []Event
			for e := cr.Archives.Front(); e != nil; e = e.Next() {
				events = append(events, e.Value.(Event))
			}
			subscriber := make(chan Event, 10)
			cr.Subscribers.PushBack(subscriber)
			ch <- Subscription{events, subscriber}

		case event := <-cr.publish:
			for ch := cr.Subscribers.Front(); ch != nil; ch = ch.Next() {
				ch.Value.(chan Event) <- event
			}
			if cr.Archives.Len() >= archiveSize {
				cr.Archives.Remove(cr.Archives.Front())
			}
			cr.Archives.PushBack(event)

		case unsub := <-cr.unsubscribe:
			for ch := cr.Subscribers.Front(); ch != nil; ch = ch.Next() {
				if ch.Value.(chan Event) == unsub {
					cr.Subscribers.Remove(ch)
					break
				}
			}
		}
	}

}

func Make() *Chatroom {
	return MakeRoomWithName("Mew Default")
}

func MakeRoomWithName(name string) *Chatroom {

	cr := &Chatroom{}
	cr.Name = name
	cr.Archives = list.New()
	cr.Subscribers = list.New()
	cr.subscribe = make(chan (chan<- Subscription), 10)
	cr.unsubscribe = make(chan (<-chan Event), 10)
	cr.publish = make(chan Event, 10)

	go cr.RunChatroom()
	return cr

}

//drain channel of messages
func drain(ch <-chan Event) {
	for {
		select {
		case _, ok := <-ch:
			if !ok {
				return
			}
		default:
			return

		}
	}
}
