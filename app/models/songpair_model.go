package models

type SongPairModel struct {
	SongPairId int
	SonglistId int
	SongId     int
	NextSong   int
	PrevSong   int
}
