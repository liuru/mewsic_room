package models

import "github.com/revel/revel"

type UserModel struct {
	UserModelId         int
	UserModelname       string
	Password       string
	HashedPassword []byte
}

func (user *UserModel) Validate(v *revel.Validation) {
	v.Check(user.UserModelname,
		revel.Required{},
		revel.MaxSize{15},
		revel.MinSize{4},
	)

	ValidatePassword(v, user.Password).
		Key("user.Password")

	v.Check(user.UserModelname,
		revel.Required{},
		revel.MaxSize{100},
	)
}

func ValidatePassword(v *revel.Validation, password string) *revel.ValidationResult {
	return v.Check(password,
		revel.Required{},
		revel.MaxSize{15},
		revel.MinSize{5},
	)
}
