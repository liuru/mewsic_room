package models

import (
	"github.com/revel/revel"
)

type SonglistModel struct {
	SonglistId    int
	SonglistName  string
	OwnerId       int
	CurrentSongId int
	HeadSongId    int
	TailSongId    int
}

//simply checks that the songlist name is fine, since we create songslists only knowing their names
func (sl *SonglistModel) Validate(v *revel.Validation) {
	v.Check(sl.SonglistName,
		revel.Required{},
		revel.MaxSize{15},
		revel.MinSize{4},
	)
}
