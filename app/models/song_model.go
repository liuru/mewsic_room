package models

type SongModel struct {
	SongId   int
	SongName string
	Duration int
}
