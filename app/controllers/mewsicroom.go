package controllers

import (
	"mewsic_room/app/chatroom"

	"code.google.com/p/go.net/websocket"
	"github.com/revel/revel"
)

type MewsicRoom struct {
	*revel.Controller
}

func (c MewsicRoom) Room(user string) revel.Result {
	return c.Render(user)
}

func (c MewsicRoom) DefaultRoomSocket(user string, ws *websocket.Conn) revel.Result {

	return c.RoomSocket(user, ws, chatroom.Default)
}

//creates a mewsic room
func (c MewsicRoom) CreateRoom(roomId int) revel.Result {
	return nil

}

//joins a mewsic room
func (c MewsicRoom) Join(roomId int) revel.Result {
	return nil

}

//leaves a mewsic room
func (c MewsicRoom) Leave(roomId int) revel.Result {
	return nil

}

//gets a mewsic room of that id
func (c MewsicRoom) GetRoom(roomId int) revel.Result {
	return nil

}

//gets all mewsic rooms
func (c MewsicRoom) AllRooms(roomId int) revel.Result {
	return nil

}

func (c MewsicRoom) RoomSocket(user string, ws *websocket.Conn, cr *chatroom.Chatroom) revel.Result {
	// Join the room.

	subscription := cr.Subscribe()
	revel.INFO.Print("Printing messages on " + cr.Name)
	defer subscription.Cancel(cr)

	cr.Join(user)
	defer cr.Leave(user)

	// Send down the archive.
	for _, event := range subscription.Archive {

		if websocket.JSON.Send(ws, &event) != nil {

			revel.INFO.Print("disconnected")
			// They disconnected
			return nil
		}
	}

	// In order to select between websocket messages and subscription events, we
	// need to stuff websocket events into a channel.
	newMessages := make(chan string)

	go func() {

		var msg string
		for {

			err := websocket.Message.Receive(ws, &msg)
			if err != nil {
				close(newMessages)
				return
			}

			revel.INFO.Print("Received message " + msg)
			newMessages <- msg
		}
	}()

	// Now listen for new events from either the websocket or the chatroom.
	for {
		select {
		case event := <-subscription.New:
			if websocket.JSON.Send(ws, &event) != nil {
				// They disconnected.
				return nil
			}

		case msg, ok := <-newMessages:
			// If the channel is closed, they disconnected.
			if !ok {
				return nil
			}
			// if the message is a text message, say it;
			cr.Say(user, msg)
		}
	}
	return nil
}
