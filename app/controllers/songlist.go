package controllers

import (
	"github.com/revel/revel"
	"encoding/json"
)

type Songlist struct {
	*revel.Controller
}

//gets every songlist
func (s Songlist) AllLists() revel.Result {

	l := getAllLists()
	return s.RenderJson(l)

}

//gets all songs
func (s Songlist) GetAllSongs() revel.Result {
    l := getAllSongs()
    return s.RenderJson(l)

}

//gets a songlist
func (s Songlist) GetSonglist(songlistId int) revel.Result {
	l := getAllSongsPairsInList(songlistId)
	return s.RenderJson(l)

}


//gets a songlist
func (s Songlist) GetSonglistInfo(songlistId int) revel.Result {
    l := getListById(songlistId)
    return s.RenderJson(l)

}




type SonglistMessage struct {
    Name string
}

//creates a songlist
func (s Songlist) CreateSonglist() revel.Result {
    var createString SonglistMessage
    
    err := json.NewDecoder(s.Request.Body).Decode(&createString)
    if err != nil {
        revel.ERROR.Println(err)
        return s.RenderXml("Error")
    }

    //TODO: use actual owner id
    createSonglist(0, createString.Name);

	return s.RenderJson(createString)
	
}

type SonglistAppendSongMessage struct {
    SongName string
}



//append a song to a songlist
func (s Songlist) AppendSong(songlistId int) revel.Result {


	var createString SonglistAppendSongMessage;
    
    err := json.NewDecoder(s.Request.Body).Decode(&createString)
    if err != nil {
        revel.ERROR.Println(err)
        return s.RenderXml("Error")
    }

    //TODO: use actual owner id

    lookupAndAppendSong(0, songlistId, createString.SongName);

	return s.RenderJson(createString)

}


type SonglistInsertSongMessage struct {
    SongName string
    SongIdx int
}

//insert a song to a songlist
func (s Songlist) InsertSong(songlistId int) revel.Result {

	var createString SonglistInsertSongMessage;
    
    err := json.NewDecoder(s.Request.Body).Decode(&createString)
    if err != nil {
        revel.ERROR.Println(err)
        return s.RenderXml("Error")
    }
    //TODO: use actual owner id
    lookupAndInsertSong(0, songlistId, createString.SongName, createString.SongIdx);

	return s.RenderJson(createString)

}


type SonglistDeleteSongMessage struct {
    SongIdx int
}
//delete a song from a songlist
func (s Songlist) DeleteSong(songlistId int) revel.Result {

	var createString SonglistDeleteSongMessage;
    
    err := json.NewDecoder(s.Request.Body).Decode(&createString)
    if err != nil {
        revel.ERROR.Println(err)
        return s.RenderXml("Error")
    }

    //TODO: use actual owner id

    lookupAndDeleteSong(0, songlistId, createString.SongIdx);

    return s.RenderJson(createString)

}

//overwrites a songlist with a new one
func (s Songlist) Clobber(songlistId int) revel.Result {

	return nil

}

//deletes a songlist
func (s Songlist) DeleteList(songlistId int) revel.Result {

    //TODO: user actual owner id
    deleteSonglist(0, songlistId)
	return s.RenderJson("Deleted list")

}
