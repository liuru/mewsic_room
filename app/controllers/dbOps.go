package controllers


//0 is not a valid id for a song

import (
	"github.com/revel/revel"
	"mewsic_room/app/models"
	"strconv"
)

const (
	SongExistsError       = "This song already exists!"
	SongDoesNotExistError = "This song does not exist!"
)

func getAllSongs()[]models.SongModel{
	var songs []models.SongModel
	Dbm.Select(&songs, "select * from SongModel")
	return songs
}

func getAllSongsPairsInList(songlistId int)[]models.SongPairModel{
	var songpairs []models.SongPairModel
	Dbm.Select(&songpairs, "select * from SongPairModel where SonglistId=" + strconv.Itoa(songlistId))
	return songpairs
}

func getAllLists() []models.SonglistModel {
	var songlists []models.SonglistModel
	Dbm.Select(&songlists, "select * from SonglistModel")
	return songlists

}

func getListById(songlistId int) models.SonglistModel {

	var songlist models.SonglistModel
	if err := Dbm.SelectOne(&songlist, "select * from SonglistModel where SonglistId=" + strconv.Itoa(songlistId)); err != nil {
		panic(err)
	}
	return songlist

}

func createSonglist(ownerId int, name string) models.SonglistModel {

	newSonglist := &models.SonglistModel{0, name, ownerId, 0, 0, 0}
	if err := Dbm.Insert(newSonglist); err != nil {
		panic(err)
	}
	return *newSonglist
}

func deleteSonglist(ownerId int, songlistId int) {
	//go through and delete all the pairs associated with the songlist
	var songs[]models.SongModel
	songlist := getListById(songlistId)
	if songlist.HeadSongId != 0{
		if _, err := Dbm.Select(&songs, "select * from SongModel where SonglistId =" + strconv.Itoa(songlistId)); err != nil{
			//We can technically have songlist without songs, but then headId should not be 0
			panic(err)
		}
		for _, song := range songs{
			Dbm.Delete(&song)
		}
	}
	if _, err := Dbm.Delete(&songlist); err != nil {
		panic(err)
	}
}


func getSongPairByPairId(songlistId int, songPairId int) models.SongPairModel{
	songpair := models.SongPairModel{}
	if err := Dbm.SelectOne(&songpair, "select * from SongPairModel where SonglistId = "+strconv.Itoa(songlistId) + " and SongPairId = " + strconv.Itoa(songPairId)); err != nil {
		panic(err)
	}
	return songpair
}


func lookupAndAppendSong(ownerId int, songlistId int, songName string){

    //TODO: check youtube for song length
	song := models.SongModel{};
	var songpairs []models.SongModel
	Dbm.Select(&songpairs, "select * from SongModel")

	revel.INFO.Println(songpairs)

	if err := Dbm.SelectOne(&song, "select * from SongModel where SongName =?", songName); err != nil{
		//insert the song
		newSong := &models.SongModel{0, songName, 0}
		if err := Dbm.Insert(newSong); err != nil {
			panic(err)
		}

		Dbm.SelectOne(&song, "select * from SongModel where SongName =?", songName);
		appendSong(ownerId, songlistId, song.SongId)

	} else {
		appendSong(ownerId, songlistId, song.SongId)

	}
}


func lookupAndInsertSong(ownerId int, songlistId int, songName string, songIdx int){

    //TODO: check youtube for song length
	song := models.SongModel{};
	var songpairs []models.SongModel
	Dbm.Select(&songpairs, "select * from SongModel")

	revel.INFO.Println(songpairs)

	if err := Dbm.SelectOne(&song, "select * from SongModel where SongName =?", songName); err != nil{
		//insert the song
		newSong := &models.SongModel{0, songName, 0}
		if err := Dbm.Insert(newSong); err != nil {
			panic(err)
		}

		Dbm.SelectOne(&song, "select * from SongModel where SongName =?", songName);
		insertSong(ownerId, songlistId, song.SongId, songIdx)

	} else {
		insertSong(ownerId, songlistId, song.SongId, songIdx)

	}
}

func insertSong(ownerId int, songlistId int, songId int, songIdx int) {

	revel.ERROR.Println(songIdx)
	//check whether this songpair already exists
	//if the thing doesn't exist, this query should return an error
	songpair := models.SongPairModel{}

	var songpairs []models.SongPairModel
	Dbm.Select(&songpairs, "select * from SongPairModel")

	revel.INFO.Println(songpairs)



	//this should only happen when we don't have the songpair
	if err := Dbm.SelectOne(&songpair, "select * from SonglistModel where SonglistId ="+strconv.Itoa(songlistId)+"and SongId ="+strconv.Itoa(songId) + "and OwnerId =" +strconv.Itoa(ownerId)); err != nil{
		songlist := getListById(songlistId)


		//NOTE: THESE ARE PAIR Ids, NOT SONG Ids
		newSongPair_ := &models.SongPairModel{0, songlistId, songId, 0, 0}
		if err := Dbm.Insert(newSongPair_); err != nil {
			panic(err)
		}
		//we have to do this or else update operations will not work
		newSongPair := getSongPairByPairId(songlistId, newSongPair_.SongPairId)

		if songlist.HeadSongId == 0{
			if (songIdx != 0){
				panic("trying to insert in an invalid location in empty list")
			}
			songlist.HeadSongId = newSongPair.SongPairId
			songlist.TailSongId = newSongPair.SongPairId
		} else if songlist.TailSongId == songlist.HeadSongId {

			if (songIdx > 1){
				panic("trying to insert in an invalid location in a list of one element")
			}

			// Inserting behind the element
			if(songIdx == 1){
				old_head := getSongPairByPairId(songlistId, songlist.HeadSongId)
				old_head.NextSong = newSongPair.SongPairId
				newSongPair.PrevSong = old_head.SongPairId
				songlist.TailSongId = newSongPair.SongPairId
				
				Dbm.Update(&old_head)
				Dbm.Update(&newSongPair)
			// inserting in front of the element
			} else {
				old_head := getSongPairByPairId(songlistId, songlist.HeadSongId)
				old_head.PrevSong = newSongPair.SongPairId
				newSongPair.NextSong = old_head.SongPairId
				songlist.HeadSongId = newSongPair.SongPairId
				
				Dbm.Update(&old_head)
				Dbm.Update(&newSongPair)

			}

		} else if songIdx == 0{
			old_head := getSongPairByPairId(songlistId, songlist.HeadSongId)
			old_head.PrevSong = newSongPair.SongPairId
			newSongPair.NextSong = old_head.SongPairId
			songlist.HeadSongId = newSongPair.SongPairId
				
			Dbm.Update(&old_head)
			Dbm.Update(&newSongPair)

		} else {
			old_tail := songPairGivenIdx(songlistId, songIdx - 1)
			curPairId := old_tail.NextSong
			old_tail.NextSong = newSongPair.SongPairId
			newSongPair.NextSong = curPairId
			newSongPair.PrevSong = old_tail.SongPairId


			//update the tail of the list if we're at the endx
			if(curPairId == 0){
				songlist.TailSongId = newSongPair.SongPairId
			//update the next pair
			} else {
				next_song := getSongPairByPairId(songlistId, curPairId)
				next_song.PrevSong = newSongPair.SongPairId
				Dbm.Update(&next_song)
			}

			//
			Dbm.Update(&old_tail)
			Dbm.Update(&newSongPair)
		}

		//get the new songpair
	
		if _, err := Dbm.Update(&songlist); err != nil {
			panic(err)
		}

	}
}

func songPairGivenIdx(songlistId int, songIdx int) models.SongPairModel{
	songlist := getListById(songlistId)
	curPairId := songlist.HeadSongId
	old_tail := getSongPairByPairId(songlistId, curPairId)

	for i := 0; i < songIdx; i++{
		if(curPairId == 0){
			panic("could not find songid given index!")
		}
		old_tail = getSongPairByPairId(songlistId, curPairId)	
		curPairId = old_tail.NextSong;
	}
	return old_tail;
}


func appendSong(ownerId int, songlistId int, songId int) {
	//check whether this songpair already exists
	//if the thing doesn't exist, this query should return an error
	
	songpair := models.SongPairModel{}

	//this should only happen when we don't have the songpair
	if err := Dbm.SelectOne(&songpair, "select * from SonglistModel where SonglistId ="+strconv.Itoa(songlistId)+"and SongId ="+strconv.Itoa(songId) + "and OwnerId =" +strconv.Itoa(ownerId)); err != nil{
		songlist := getListById(songlistId)


		//NOTE: THESE ARE PAIR Ids, NOT SONG Ids
		newSongPair_ := &models.SongPairModel{0, songlistId, songId, 0, 0}
		if err := Dbm.Insert(newSongPair_); err != nil {
			panic(err)
		}


		//we have to do this or else update operations will not work
		newSongPair := getSongPairByPairId(songlistId, newSongPair_.SongPairId)

		if songlist.HeadSongId == 0{
			songlist.HeadSongId = newSongPair.SongPairId
			songlist.TailSongId = newSongPair.SongPairId
		} else if songlist.TailSongId == songlist.HeadSongId {
			songlist.TailSongId = newSongPair.SongPairId
			old_head := getSongPairByPairId(songlistId, songlist.HeadSongId)
			old_head.NextSong = newSongPair.SongPairId
			newSongPair.PrevSong = old_head.SongPairId
		
			Dbm.Update(&old_head)
			songlist.TailSongId = newSongPair.SongPairId
		} else{
			var songpairs []models.SongPairModel
			Dbm.Select(&songpairs, "select * from SongPairModel")
			revel.INFO.Println(songpairs)
			old_tail := getSongPairByPairId(songlistId, songlist.TailSongId)
			old_tail.NextSong = newSongPair.SongPairId
			newSongPair.PrevSong = old_tail.SongPairId
			Dbm.Update(&old_tail)
			songlist.TailSongId = newSongPair.SongPairId
		}

		//get the new songpair
	
		if _, err := Dbm.Update(&songlist); err != nil {
			panic(err)
		}

		Dbm.Update(&newSongPair)

	}
}

//func appendHelper(){}

func lookupAndDeleteSong(ownerId int, songlistId int, songIdx int){

	old_tail := songPairGivenIdx(songlistId, songIdx)
	deleteSong(ownerId, songlistId, old_tail.SongPairId)

}

func deleteSong(ownerId int, songlistId int, songPairId int) {
	//check whether this songpair already exists
	songpair := models.SongPairModel{}

	_, err := Dbm.Select(&songpair, "select * from SongPairModel where ownerId=" + strconv.Itoa(ownerId) + " and SongPairID ="+strconv.Itoa(songPairId)+" and SonglistId = "+strconv.Itoa(songlistId))
	if err != nil {
		panic(err)
	} else {
		
		// Connect the prev song to the next song.
		if songpair.PrevSong != 0 && songpair.NextSong != 0 {
			prev := getSongPairByPairId(songlistId, songpair.PrevSong)	
			next := getSongPairByPairId(songlistId, songpair.NextSong)
			prev.NextSong = next.SongPairId;
			next.PrevSong = prev.SongPairId;
			Dbm.Update(&prev)
			Dbm.Update(&next)

		// This is the end of the list.	
		} else if songpair.PrevSong != 0 {
			prev := getSongPairByPairId(songlistId, songpair.PrevSong)
			prev.NextSong = 0
			Dbm.Update(&prev)
			songlist := getListById(songlistId)
			songlist.TailSongId = prev.SongPairId
			Dbm.Update(&songlist)

		// This is the beginning of the list
		} else if songpair.NextSong != 0 {
			next := getSongPairByPairId(songlistId, songpair.NextSong)
			next.PrevSong = 0
			Dbm.Update(&next)
			songlist := getListById(songlistId)
			songlist.HeadSongId = next.SongPairId
			Dbm.Update(&songlist)
		}

		if _, err = Dbm.Delete(songpair); err != nil {
			panic(err)
		}
	}
}

func clobberSonglist(songlistId int, newSongs []models.SonglistModel) {

}
