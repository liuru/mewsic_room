package controllers

import (
	"database/sql"
	"mewsic_room/app/models"

	"code.google.com/p/go.crypto/bcrypt"

	"github.com/coopernurse/gorp"
	_ "github.com/mattn/go-sqlite3"
	r "github.com/revel/revel"
	"github.com/revel/revel/modules/db/app"
)

var (
	Dbm *gorp.DbMap
)

func InitDB() {
	db.Init()
	Dbm = &gorp.DbMap{Db: db.Db, Dialect: gorp.SqliteDialect{}}

	/*
		setColumnSizes := func(t *gorp.TableMap, colSizes map[string]int) {
			for col, size := range colSizes {
				t.ColMap(col).MaxSize = size
			}
		}
	*/

	//users
	t := Dbm.AddTable(models.UserModel{}).SetKeys(true, "UserModelId")
	t.ColMap("Password").Transient = true

	//songlists
	t = Dbm.AddTable(models.SonglistModel{}).SetKeys(true, "SonglistId")

	//song list pairs
	t = Dbm.AddTable(models.SongPairModel{}).SetKeys(true, "SongPairId")

	//song cache
	t = Dbm.AddTable(models.SongModel{}).SetKeys(true, "SongId")

	Dbm.TraceOn("[gorp]", r.INFO)
	Dbm.CreateTables()

	bcryptPassword, _ := bcrypt.GenerateFromPassword(
		[]byte("demo"), bcrypt.DefaultCost)
	demoUserModel := &models.UserModel{0, "Demo UserModel", "demo", bcryptPassword}
	if err := Dbm.Insert(demoUserModel); err != nil {
		panic(err)
	}

	default_songlist := &models.SonglistModel{0, "default_mewlist", 0, 0, 0, 0}
	if err := Dbm.Insert(default_songlist); err != nil {
		panic(err)
	}
}

type GorpController struct {
	*r.Controller
	Txn *gorp.Transaction
}

func (c *GorpController) Begin() r.Result {
	txn, err := Dbm.Begin()
	if err != nil {
		panic(err)
	}
	c.Txn = txn
	return nil
}

func (c *GorpController) Commit() r.Result {
	if c.Txn == nil {
		return nil
	}
	if err := c.Txn.Commit(); err != nil && err != sql.ErrTxDone {
		panic(err)
	}
	c.Txn = nil
	return nil
}

func (c *GorpController) Rollback() r.Result {
	if c.Txn == nil {
		return nil
	}
	if err := c.Txn.Rollback(); err != nil && err != sql.ErrTxDone {
		panic(err)
	}
	c.Txn = nil
	return nil
}
