package controllers

import "github.com/revel/revel"
import "mewsic_room/app/routes"

type App struct {
	*revel.Controller
}

func (c App) Index() revel.Result {
	return c.Render()
}

func (c App) EnterDemo(user string) revel.Result {
	c.Validation.Required(user)

	if c.Validation.HasErrors() {
		c.Flash.Error("Please choose a nick name.")
		return c.Redirect(App.Index)
	}
	return c.Redirect(routes.MewsicRoom.Room(user))
}
