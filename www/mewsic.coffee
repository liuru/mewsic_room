doasync = (func) ->
  setTimeout ->
    func()
  , 0
From = $.Enumerable.From
join = (separator) ->
  (a, b) -> a + separator + b

typeIsArray = Array.isArray || ( value ) -> return {}.toString.call( value ) is '[object Array]'

debugObj = (obj, tag) ->
  newObj = {}
  for func of obj
    do (func) ->
      original = obj[func]
      newObj[func] = (args...) ->
        console.log "#{tag}.#{func}(" + From(args).Select(JSON.stringify).Aggregate(join(',')) + ')'
        original.apply(obj, args)
  newObj

debugFunc = (func, tag) ->
  (args...) ->
    console.log "#{tag}(" + From(args).Aggregate(join(',')) + ')'
    func(args...)

MOCK_SONGS = {
  miku1: 'bid1xo_haaM',
  miku2: '5ym66poadw0',
  miku3: 'xKtkVZuMOTk',
  miku4: 'zGarzCAQcAY',
  rctr1: 'Y_udc_Wjrvo',
  rctr2: 'vb1A2NnIcmU',
  lol1: 'R0cia14N9no',
}

COMMAND_SYNTAX = {
  createRoom : ['create room', ['name'], ['string']],
  getRoom: ['get room', ['id'], ['int']],
  joinRoom: ['join room', ['id'], ['int']],
  leaveRoom: ['leave room', ['id'], ['int']],
  getRooms: ['get all rooms', [], []],
  getSonglists: ['get all songlists', [], []],
  getSonglist: ['get songlist', ['id'], ['int']],
  createSonglist: ['create songlist', ['name'], ['string']],
  appendSong: ['append to songlist', ['songlist_id', 'song_id'], ['int', 'string']],
  insertSong: ['insert to songlist', ['songlist_id', 'song_id', 'at_index'], ['int', 'string', 'int']],
  deleteSong: ['delete from songlist', ['songlist_id', 'song_id'], ['int', 'string']],
  clobberSonglist: ['rearrange songlist', ['songlist_id', 'index1,index2,...'], ['int', ['int']]],
  deleteSonglist: ['delete songlist', ['songlist_id'], ['int']],
}

getMockApi = ->
  mockRooms =
    1: {id: 1, name: "Room 1", users: ['hpmv', 'rurulu']}

  mockSonglists =
    1: {id: 1, owner: 'hpmv', default: true, name: 'squeaky girl', songs: [
      MOCK_SONGS.miku1, MOCK_SONGS.miku2, MOCK_SONGS.miku3, MOCK_SONGS.miku4]}

  api = {}
  api.createRoom = (name, cb) ->
    doasync -> cb(1)
  api.getRoom = (id, cb) ->
    doasync -> cb mockRooms[id]
  api.joinRoom = api.getRoom
  api.leaveRoom = (id, cb) ->
    doasync -> cb()
  api.getRooms = (cb) ->
    doasync -> cb(From(Object.keys(mockRooms)).Select((x)->mockRooms[x]).ToArray())

  api.getSonglists = (cb) ->
    doasync -> cb [mockSonglists[1]]
  api.getSonglist = (id, cb) ->
    doasync -> cb mockSonglists[1]
  api.createSonglist = (name, cb) ->
    doasync -> cb()
  api.appendSong = (listId, songId, cb) ->
    mockSonglists[1].songs.push(songId)
    doasync -> cb()
  api.insertSong = (listId, songId, index, cb) ->
    mockSonglists[1].songs.splice(index, 0, songId)
    doasync -> cb()
  api.deleteSong = (listId, songId, cb) ->
    mockSonglists[1].songs.splice(mockSonglists[1].indexOf(songId), 1)
  api.clobberSonglist = (listId, data, cb) ->
    mockSonglists[1].songs = data
    doasync -> cb()
  api.deleteSonglist = (listId, cb) ->
    doasync -> cb()

  return debugObj(api, 'mockApi')

getRealApi = ->
  post = (path, data, cb) ->
    $.post path, data, cb, 'json'
  get = (path, cb) ->
    $.get path, cb, 'json'
  del = (path, cb) ->
    $.ajax
      type: 'DELETE',
      url: path
      success: cb
      dataType: 'json'

  api = {}
  api.createRoom = (name, cb) ->
    post '/room/create', name, cb
  api.getRoom = (id, cb) ->
    get "/room/#{id}", cb
  api.leaveRoom = (id, cb) ->
    post "/room/leave/#{id}", cb
  api.getRooms = (cb) ->
    get "/rooms", cb

  api.getSonglists = (cb) ->
    get "/songlists", cb
  api.getSonglist = (listId, cb) ->
    get "/songlist/#{listId}", cb
  api.createSonglist = (name, cb) ->
    post "/songlist/create", name, cb
  api.appendSong = (listId, songId, cb) ->
    post "/songlist/append/#{listId}", songId, cb
  api.insertSong = (listId, songId, index, cb) ->
    post "/songlist/insert/#{listId}", {songID: songId, index: index}, cb
  api.deleteSong = (listId, songId, cb) ->
    post "/songlist/delete/#{listId}", songId, cb
  api.clobberSonglist = (listId, data, cb) ->
    post "/songlist/clobber/#{listId}", data, cb
  api.deleteSonglist = (listId, cb) ->
    del "/songlist/#{listId}", cb

  return debugObj(api, 'api')


getCli = ->
  cli = {}
  cli.showMsg = (from, type, msg) ->
    $('#cli').append(
      $('<div>').addClass('cli_msg').addClass("cli_type_#{type}").append(
        $('<div>').addClass('cli_from').text(from)
      ).append(
        $('<div>').addClass('cli_content').html(msg)
      )
    )
    $('#cli').stop().animate({
      scrollTop: $("#cli")[0].scrollHeight
    }, 400);
  debugObj(cli, 'cli')

getCliUI = (cli) ->
  ui = {}
  ui.showRoom = (room) ->
    cli.showMsg 'Room', 'system',
      "[Room ##{room.id}] #{room.name} with #{room.users.length} users:<br>" +
        From(room.users).Aggregate join('<br>')
  ui.showRoomCreated = (id, name) ->
    cli.showMsg 'Room', 'system',
      "Room ##{id} : #{name} created."
  ui.showLeftRoom = (id) ->
    cli.showMsg 'Room', 'system',
      "You have left room ##{id}"
  ui.showRooms = (rooms) ->
    cli.showMsg 'Rooms', 'system',
      From rooms
      .Select (room) -> "[#{room.id}] #{room.name} (#{room.users.length} users)"
      .Aggregate join('<br>')
  ui.showSonglists = (songlists) ->
    cli.showMsg 'Songlists', 'system',
      From songlists
      .Select (songlist) -> "[#{songlist.id}] #{songlist.name} (#{songlist.songs.length} songs)"
      .Aggregate join('<br>')
  ui.showSelectedSonglist = (songlist) ->
    cli.showMsg 'Selected Songlist', 'system',
      From songlist.songs
      .Select (song) -> song
      .Aggregate join('<br>')
  ui.showSonglist = (songlist) ->
    cli.showMsg 'Songlist', 'system',
      "[Songlist ##{songlist.id}] #{songlist.name}: with #{songlist.songs.length} songs:<br>" +
        From songlist.songs
        .Select (song) -> song
        .Aggregate join('<br>')
  ui.showSonglistCreated = (id, name) ->
    cli.showMsg 'Songlist', 'system',
      "Song list ##{id} : #{name} created."
  ui.showSonglistDeleted = (id) ->
    cli.showMsg 'Songlist', 'system',
      "Song list ##{id} deleted."
  debugObj(ui, 'ui')

getCliPrintingApi = (innerApi, cliUi) ->
  api = {}
  api.createRoom = (name, cb) ->
    await innerApi.createRoom name, defer id
    cb(id)
    cliUi.showRoomCreated id, name
  api.getRoom = (id, cb) ->
    await innerApi.getRoom id, defer room
    cb(room)
    cliUi.showRoom(room)
  api.leaveRoom = (id, cb) ->
    await innerApi.leaveRoom name, defer ignored
    cb(ignored)
    cliUi.showLeftRoom(id)
  api.getRooms = (cb) ->
    await innerApi.getRooms defer rooms
    cb(rooms)
    cliUi.showRooms(rooms)
  api.getSonglists = (cb) ->
    await innerApi.getSonglists defer songlists
    cb(songlists)
    cliUi.showSonglists(songlists)
  api.getSonglist = (listId, cb) ->
    await innerApi.getSonglist listId, defer songlist
    cb(songlist)
    cliUi.showSonglist(songlist)
  api.createSonglist = (name, cb) ->
    await innerApi.createSonglist name, defer id
    cb(id)
    cliUi.showSonglistCreated(id, name)
  api.appendSong = (listId, songId, cb) ->
    await innerApi.appendSong listId, songId, defer ignored
    cb(ignored)
    await innerApi.getSonglist listId, defer songlist
    cliUi.showSonglist(songlist)
  api.insertSong = (listId, songId, index, cb) ->
    await innerApi.insertSong listId, songId, index, defer ignored
    cb(ignored)
    await innerApi.getSonglist listId, defer songlist
    cliUi.showSonglist(songlist)
  api.deleteSong = (listId, songId, cb) ->
    await innerApi.deleteSong listId, songId, defer ignored
    cb(ignored)
    await innerApi.getSonglist listId, defer songlist
    cliUi.showSonglist(songlist)
  api.clobberSonglist = (listId, data, cb) ->
    await innerApi.clobberSonglist listId, data, defer ignored
    cb(ignored)
    await innerApi.getSonglist listId, defer songlist
    cliUi.showSonglist(songlist)
  api.deleteSonglist = (listId, cb) ->
    await innerApi.deleteSonglist listId, defer ignored
    cb(ignored)
    cliUi.showSonglistDeleted(listId)

  return api

getMockWebSocketApi = (cb) ->
  doasync -> cb(null)  #todo

getCommandProcessor = ->
  ac = {}
  prefixes = {}
  command_strings = {}
  for command, [prefix, args, types] of COMMAND_SYNTAX
    prefixes[prefix] = command;
    cstr = prefix
    for i in [0...args.length]
      cstr += ' ' + args[i]
      cstr += if typeof(types[i]) == 'string' then ':' + types[i] else ''
    command_strings[command] = cstr

  ac.query = (current) ->
    includedCommands = []
    for prefix of prefixes
      if current.indexOf(prefix) == 0
        includedCommands.push(prefixes[prefix])
      else if prefix.indexOf(current) == 0
        includedCommands.push(prefixes[prefix])
    for command in includedCommands
      command_strings[command]

  argParse = (arg, type) ->
    switch
      when type == 'int'
        i = parseInt(arg)
        return if isNaN(i) then null else i
      when type == 'string'
        return if arg.length > 0 then arg else null
      when typeIsArray type
        return argParse(x, type[0]) for x in arg.split(',')  #replace with better splitting
      else
        return null

  ac.parse = (str) ->
    for prefix of prefixes
      command = prefixes[prefix]
      syntax = COMMAND_SYNTAX[command]
      if syntax[2].length == 0 and str.indexOf(prefix) == 0
        return command: command, args: {}, argsInOrder: []
      if str.indexOf(prefix + ' ') == 0
        try
          argsStr = str.substring(prefix.length + 1)
          args = argsStr.split(' ')  #replace with good parsing :)
          parsedArgs =
            for i in [0...syntax[2].length]
              argParse(args[i], syntax[2][i])
          if From(parsedArgs).Any((a) -> a == null)
            continue
          else
            obj = {}
            for parsedArg, i in parsedArgs
              obj[syntax[1][i]] = parsedArg
            return command: command, args: obj, argsInOrder: parsedArgs
        catch
          continue
    return null

  ac.executeCommand = (command, argsInOrder, api, cb) ->
    api[command]((argsInOrder.concat [cb])...)

  debugObj(ac, 'ac')

init = (cb) ->
  api = getMockApi()
  cli = getCli()
  ui = getCliUI(cli)
  # await an object to still be valid vanilla coffeescript syntax
  await
    1: api.getRooms defer rooms
    2: api.getSonglists defer songlists
  ui.showRooms rooms
  ui.showSonglists songlists
  selectedSonglist = From(songlists).Where((b) -> b.default).First()
  ui.showSelectedSonglist selectedSonglist
  await getMockWebSocketApi(defer socket)
  cb(getCliPrintingApi(api, ui), socket, cli, ui)

$ ->
  await init defer api, socket, cli, ui
  cli.showMsg('Ready', 'system', 'Enjoy your mewsic.')
  ac = getCommandProcessor()
  $('#command-box').autocomplete
    source: (req, resp) ->
      resp ac.query(req.term)
  $('#command-box').keyup ->
    parsed = ac.parse $('#command-box').val()
    console.log parsed
    if parsed != null
      $('#command-box').addClass('good-command')
    else
      $('#command-box').removeClass('good-command')
  $('#command-box').keydown (e) ->
    if e.which == 13
      parsed = ac.parse $('#command-box').val()
      if parsed != null
        ac.executeCommand parsed.command, parsed.argsInOrder, api, (d) ->
          console.log d