/* POST FUNCTIONS */

function createList(name){
	var jsonData = {};
	jsonData["Name"] = name;
	var xhr = new XMLHttpRequest();
	xhr.open("post", "/songlist/create");
	xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
	xhr.send(JSON.stringify(jsonData));
}

function appendSong(songlistId, songName){

	var jsonData = {};
	jsonData["SongName"] = songName;
	var xhr = new XMLHttpRequest();
	xhr.open("post", "/songlist/append/" + songlistId);
	xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
	xhr.send(JSON.stringify(jsonData));

}

function insertSong(songlistId, songName, songIdx){

	var jsonData = {};
	jsonData["SongName"] = songName;
	jsonData["SongIdx"] = songIdx;
	var xhr = new XMLHttpRequest();
	xhr.open("post", "/songlist/insert/" + songlistId);
	xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
	xhr.send(JSON.stringify(jsonData));

}

/* GET FUNCTIONS */


/* DELETE FUNCTIONS */